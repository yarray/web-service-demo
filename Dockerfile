FROM python:3.8
RUN pip3 install fastapi uvicorn
COPY ./* $HOME/
CMD ["python3", "app.py"]
